public class Kata {
    public static void main(String[] args) {
        String M = "world";
        String N = "word";

        String palabra1Invertida = invertirPalabra(M);

        String palabra2Invertida = invertirPalabra(N);

        System.out.println("Palabra original world: " + M);
        System.out.println("Palabra invertida world: " + palabra1Invertida);

        System.out.println("Palabra original word: " + N);
        System.out.println("Palabra Invertida word: " + palabra2Invertida);
    }

    public static String invertirPalabra(String palabra) {
        StringBuilder palabraRevertida = new StringBuilder(palabra);
        palabraRevertida.reverse();
        return palabraRevertida.toString();
    }
}