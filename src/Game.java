public class Game {
    public static void main(String[] args) {
        Game game = new Game();

        String[] deckSteve = {"A", "7", "8"};
        String[] deckJosh = {"K", "5", "9"};

        String result = game.winner(deckSteve, deckJosh);
        System.out.println(result);
    }

    public String winner(String[] deckSteve, String[] deckJosh) {
        int AS = 0;
        int AJ = 0;

        for (int i = 0; i < deckSteve.length; i++) {
            int steveRank = rank(deckSteve[i]);
            int joshRank = rank(deckJosh[i]);

            if (steveRank > joshRank) {
                AS++;
            } else if (joshRank > steveRank) {
                AJ++;
            }
        }

        if (AS > AJ) {
            return "Steve wins " + AS + " to " + AJ;
        } else if (AJ > AS) {
            return "Josh wins " + AJ + " to " + AS;
        } else {
            return "Tie";
        }
    }

    private int rank(String card) {
        switch (card) {
            case "2":
                return 2;
            case "3":
                return 3;
            case "4":
                return 4;
            case "5":
                return 5;
            case "6":
                return 6;
            case "7":
                return 7;
            case "8":
                return 8;
            case "9":
                return 9;
            case "T":
                return 10;
            case "J":
                return 11;
            case "Q":
                return 12;
            case "K":
                return 13;
            case "A":
                return 14;
            default:
                throw new IllegalArgumentException("Invalid card: " + card);
        }
    }
}